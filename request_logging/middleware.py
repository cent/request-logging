import json
import logging
import socket

import time

from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
from rest_framework.utils import encoders


logger = logging.getLogger('request-api')


PREFIX_HTTP_HEADER = 'HTTP_'
LEN_PREFIX_HTTP_HEADER = len(PREFIX_HTTP_HEADER)


def normalize_header_name(name):
    if name.startswith(PREFIX_HTTP_HEADER):
        name = name[LEN_PREFIX_HTTP_HEADER:]
    return '-'.join((n.capitalize() for n in name.lower().split('_')))


def format_json(data):
    try:
        return json.dumps(json.loads(data), cls=encoders.JSONEncoder, indent=2)
    except Exception:
        if data is None or data == 'None':
            return ''
        return data


class RequestLogMiddleware(MiddlewareMixin):
    LOGGING_REQUEST_PREFIX_PATH = getattr(settings, 'LOGGING_REQUEST_PREFIX_PATH', '/')

    FORMAT_MESSAGE = '---=== Request user={user} from ip={ip} to server_host={server_host} time={delta} ===---\n\n' \
                      '{req_text}\n\n{resp_text}\n{delim}'

    def process_request(self, request):
        if not self.LOGGING_REQUEST_PREFIX_PATH or not request.path.startswith(self.LOGGING_REQUEST_PREFIX_PATH):
            return
        try:
            self.req_text = self.format_request(request)
            self.start_time = time.time()

        except Exception:
            logger.exception('error format request')

    def process_response(self, request, response):
        if not self.LOGGING_REQUEST_PREFIX_PATH or not request.path.startswith(self.LOGGING_REQUEST_PREFIX_PATH):
            return response
        try:
            start_time = getattr(self, 'start_time', None)
            delta = round(time.time() - self.start_time, 3) if start_time else None

            req_text = getattr(self, 'req_text', None)
            resp_text = self.format_response(response)

            text = self.FORMAT_MESSAGE.format(
                user=request.user,
                ip=request.META['REMOTE_ADDR'],
                server_host=socket.gethostname(),
                delta=delta,
                req_text=req_text,
                resp_text=resp_text,
                delim='-' * 120
            )

            logger.info(text)

        except Exception:
            logger.exception('error format response')

        return response

    def format_headers(self, headers):
        return '\n  '.join(['{}: {}'.format(k, v) for k, v in headers.items()])

    def format_request(self, request):
        request_body = format_json(request.body.decode('utf-8'))

        headers = {normalize_header_name(k): v for k, v in request.META.items() if k.startswith(PREFIX_HTTP_HEADER)}
        req_text = '\n  '.join([
            '{method} {url}'.format(method=request.method, url=request.get_full_path()),
            self.format_headers(headers),
        ]) + '\n' + request_body

        return req_text

    def format_response(self, response):
        if 'content-type' in response and response['content-type'] == 'application/json':
            if getattr(response, 'streaming', False):
                response_body = '<<<Streaming>>>'
            else:
                response_body = format_json(response.content.decode('utf-8'))
        else:
            response_body = response.content.decode('utf-8')
        resp_text = '\n  '.join([
            'RESPONSE {status_code} {status_text}'.format(
                response=response,
                status_code=getattr(response, 'status_code', None),
                status_text=getattr(response, 'status_text', None)),
            self.format_headers(dict(response._headers.values())),
        ]) + '\n' + response_body
        return resp_text

