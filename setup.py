#!/usr/bin/env python
try:
    from setuptools import setup, find_packages
except ImportError:
    from distribute_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

import request_logging


setup(
    name='request-logging',
    version=request_logging.__version__,
    description='Django request logging',
    long_description='\n' + open('README').read(),
    author='Vadim Statishin',
    author_email='statishin@gmail.com',
    keywords='request api',
    license='BSD License',
    url='http://bitbucket.org/cent/request-logging/',
    #tests_require = ['nose', 'webtest'],
    #test_suite='nose.collector',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Database',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    platforms='any',
    zip_safe=True,
    packages=find_packages(),
    include_package_data=True,

#    requires=['django', 'requests'],
    install_requires=['django',],
)
